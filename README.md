Atlassian REST API Browser
==========================

The Atlassian REST API Browser add-on allows a developer to browse, discover, and test Atlassian's rich REST and JSON-RPC APIs. You can install the REST API Browser into your Atlassian product through the UPM.

To allow anonymous access to the REST API Browser, start the product with the system property 'rest.api.browser.anonymous' set to 'true'.